import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'first.settings')

import django
django.setup()

from django.core.files import File

import os
import requests
# import urllib
import tempfile
import wget
import random
from firstApp.models import Category, Product
from faker import Faker

fake_gen = Faker()
categories = ['ASD', 'BHNGSGH', 'CA', 'DWE434', 'E2D', 'F87AD_D']

def add_category():
    c = Category.objects.get_or_create(name=random.choice(categories))[0]
    c.save()
    return c

def generate(imgid):
    cat = add_category()

    fake_name = fake_gen.name()
    fake_count = random.randint(1, 100)
    fake_show = True
    fake_off = random.randint(1, 20)
    fake_price = random.randint(20, 300)
    img = requests.get('https://picsum.photos/600/300?random=1').url
    img_dir = tempfile.gettempdir() + '99' + str(imgid) + "hellohisalam.jpg"
    # urllib.request.urlretrieve(img, img_dir)
    wget.download(img, img_dir)
    fake_exp = fake_gen.paragraph(nb_sentences=3)

    pro = Product.objects.get_or_create(name=fake_name, count=fake_count, show=fake_show, off=fake_off, explanation=fake_exp, price=fake_price, category=cat)[0]
    pro.image.save(str(imgid) + "hellohisalam.jpg", File(open(img_dir, 'rb')))
    # pro.save()
    os.remove(img_dir)


N = 20
if __name__ == "__main__":
    print('Start')
    for i in range(N):
        print('#', i)
        generate(i)
    print('Done')