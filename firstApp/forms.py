from django import forms
from django.core import validators
from .models import Category, Product

# class addCategory(forms.Form):
#     name = forms.CharField()
#     explanation = forms.CharField(widget=forms.Textarea)

    # def clean(self):
    #     acd = super().clean()
    #     email = acd['email']
    #     vmail = acd['vmail']

    #     if email != vmail:
    #         raise forms.ValidationError('MAKE SURE EMAILS MATCH !')

    # def clean_botcatcher(self):
    #     botcatcher = self.cleaned_data['botcatcher']
    #     if len(botcatcher) > 0:
    #         raise forms.ValidationError('GOTCHA BOT :(')
    #     return botcatcher

class addCategory(forms.ModelForm):
    class Meta():
        model = Category
        fields = '__all__'


# class addProduct(forms.Form):
#     name = forms.CharField()
#     category = forms.ModelChoiceField(queryset=Category.objects.all())
#     count = forms.IntegerField()
#     price = forms.CharField()
#     show = forms.BooleanField(required=False)
#     off = forms.IntegerField(initial=0)
#     image = forms.ImageField()
#     explanation = forms.CharField(widget=forms.Textarea)

#     def clean(self):
#         d = self.cleaned_data['image']
#         print("#######", d)

class addProduct(forms.ModelForm):
    class Meta():
        model = Product
        fields = '__all__'