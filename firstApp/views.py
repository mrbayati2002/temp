from django.shortcuts import render
from django.http import HttpResponse
from .models import Category, Product
from django.conf import settings
from . import forms

# Create your views here.

def one(request):
    return HttpResponse('This is ONE')

def two(request):
    return render(request, 'firstApp/two.html')

def home(request):
    payload = {}
    return render(request, 'home.html', context=payload)

def products(request):
    prs = Product.objects.all()
    data = {'prs': prs,
            'media': settings.MEDIA_URL,
            'noProducts': len(prs),
            }
    return render(request, 'firstApp/products.html', data)

def addCategory(request):
    form = forms.addCategory()

    if request.method == 'POST':
        form = forms.addCategory(request.POST)
        if form.is_valid():
            form.save(commit=True)
            return home(request)
        else:
            print('ERROR FORM INVALID')
    return render(request, 'firstApp/addCategory.html', {'form':form})

def addProduct(request):
    form = forms.addProduct()

    if request.method == 'POST':
        form = forms.addProduct(request.POST, request.FILES)
        if form.is_valid():
            form.save(commit=True)
            return home(request)
        else:
            print('ERORR FROM INVALID')
    return render(request, 'firstApp/addProduct.html', {'form':form})