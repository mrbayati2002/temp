from django.db import models

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=150, unique=True)
    explanation = models.TextField(blank=True)

    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=150)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    count = models.IntegerField()
    price = models.CharField(max_length=100)
    show = models.BooleanField()
    off = models.IntegerField()
    image = models.ImageField(upload_to='image/', blank=False, null=True,)
    explanation = models.TextField(blank=True)

    def __str__(self):
        return (f'{self.name} : {self.count} : {self.price}')