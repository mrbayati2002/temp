from django.urls import include, path
from firstApp import views

app_name = 'firstApp'
urlpatterns = [
    # re_path(r'^$', views.index, name='index'),
    path('one', views.one, name='one'),
    path('two', views.two, name='two'),
    path('', views.home, name='home'),
    path('products/', views.products, name='products'),
    path('add_category', views.addCategory, name='addCategory'),
    path('add_product', views.addProduct, name='addProduct'),
]
